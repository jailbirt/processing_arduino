#include "FastSPI_LED2.h"

///////////////////////////////////////////////////////////////////////////////////////////
//
// Move a dot along the strip of leds.  This program simply shows how to configure the leds,
// and then how to turn a single pixel white and then off, moving down the line of pixels.
// 

// How many leds are in the strip?
#define NUM_LEDS 32
// Data pin that led data will be written out over
#define DATA_PIN 3
// Clock pin only needed for SPI based chipsets when not using hardware SPI
#define CLOCK_PIN 2
// This is an array of leds.  One item for each led in your strip.
CRGB leds[NUM_LEDS];


//Serial Part
char val; // Data received from the serial port



// This function sets up the ledsand tells the controller about them
void setup() {
       // sanity check delay - allows reprogramming if accidently blowing power w/leds
       delay(2000);
       LEDS.setBrightness(2); //Brillo, para que no me vuele los ojos.
       FastLED.addLeds<WS2801, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
       Serial.begin(9600); // Start serial communication at 9600 bps
       
}

// This function runs over and over, and is where you do the magic to light
// your leds.
void loop() {
   
     readSerial();
     if (val) { // If val was received
      trailSerialWithLight(val);
      int reverseVal=32-val;
      trailSerialWithLight(reverseVal);
      Serial.println("val"+String(val)+" reverse"+reverseVal);
    } 
  }

void readSerial() {
  
  if(Serial.available() > 0 ) {
     val= Serial.read();
  }
 }

void trailSerialWithLight(char val){
     int ledActual=val;
     // Turn our current led on then show the leds
      leds[ledActual] = CRGB::Blue;
      if (ledActual-1 >= 0){
        leds[ledActual-1] = CRGB::Cyan;     
      }
      if (ledActual-2 >= 0){
        leds[ledActual-2] = CRGB::Magenta;     
      }
      // Show the leds (only one of which is set, from above)
      FastLED.show();

      // Wait a little bit
      delay(1);
      
      // Turn our current led back to black for the next loop around
      leds[ledActual] = CRGB::Black;
      if (ledActual >= 1){
        leds[ledActual-1] = CRGB::Black;     
      }
      if (ledActual >= 2){
        leds[ledActual-2] = CRGB::Black;     
      }
 //   for(int ledActual = 0; ledActual < NUM_LEDS; ledActual = ledActual + 1) {      
//}
}
