#include "FastSPI_LED2.h"

///////////////////////////////////////////////////////////////////////////////////////////
//
// Move a dot along the strip of leds.  This program simply shows how to configure the leds,
// and then how to turn a single pixel white and then off, moving down the line of pixels.
// 

// How many leds are in the strip?
#define NUM_LEDS 32
// Data pin that led data will be written out over
#define DATA_PIN 3
// Clock pin only needed for SPI based chipsets when not using hardware SPI
#define CLOCK_PIN 2
// This is an array of leds.  One item for each led in your strip.
CRGB leds[NUM_LEDS];

//Serial Part
int valX1,valX2,valX3,valX4,valX5,valX6,valX7,valX8,valX9,valX10,valY=0; // Data received from the serial port
int currentValue = 0;
int values[] = {
  0,0};

// This function sets up the ledsand tells the controller about them
void setup() {
  // sanity check delay - allows reprogramming if accidently blowing power w/leds
  delay(2000);
  LEDS.setBrightness(2); //Brillo, para que no me vuele los ojos.
  FastLED.addLeds<WS2801, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
  Serial.begin(9600); // Start serial communication at 9600 bps

}

// This function runs over and over, and is where you do the magic to light
// your leds.
void loop() {

  readSerial();
  if (valX1) { // If val was received
    trailSerialWithLight(valX1);
  }
  if (valY) {
    LEDS.setBrightness(valY); //Brillo
  }  
}

void readSerial() {

  if(Serial.available()){
    int incomingValue = Serial.read();

    values[currentValue] = incomingValue;

    currentValue++;
    if(currentValue > 1){
      currentValue = 0;
    }
    // After this point values[]
    // has the most recent set of
    // all values sent in from Processing

    valX1=values[0];
    valY=values[1];
    Serial.print("VALORES: ");
    Serial.print(valX1);
    Serial.print("|");
    Serial.print(valY);
  } 

}

void trailSerialWithLight(char val){
  int ledActual=val;
  // Turn our current led on then show the leds
  leds[ledActual] = CRGB::Blue;
  // Show the leds (only one of which is set, from above)
  FastLED.show();
  // Wait a little bit
  delay(1);
  // Turn our current led back to black for the next loop around
  leds[ledActual] = CRGB::Black;
}


