#---
# Excerpted from "Programming Your Home",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/mrhome for more book information.
#---
# Be sure to restart your server when you modify this file.

# Your secret key for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!
# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
X10switch::Application.config.secret_token = '5aa6d16464c791a970a064c5d94a8b38a63ee9f9088dcc430655f2f4307b0345ac33f7e38d5f19be2c02d09e150ffe0da43ee86997a05b511172a4e98f0a21af'
