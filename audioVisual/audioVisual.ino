#include "FastSPI_LED2.h"

///////////////////////////////////////////////////////////////////////////////////////////
//
// Move a dot along the strip of leds.  This program simply shows how to configure the leds,
// and then how to turn a single pixel white and then off, moving down the line of pixels.
// 

// How many leds are in the strip?
#define NUM_LEDS 64
// Data pin that led data will be written out over
#define DATA_PIN 3
// Clock pin only needed for SPI based chipsets when not using hardware SPI
#define CLOCK_PIN 2
// This is an array of leds.  One item for each led in your strip.
CRGB leds[NUM_LEDS];


int micPin = 0;
int val=0,valant=0,sensorvalue=0,lastsensorvalue=0,lastminsensorvalue=1023;

int potenciometerPin = 3;     // potentiometer wiper (middle terminal) connected to analog pin 3
                       // outside leads to ground and +5V

// This function sets up the ledsand tells the controller about them
void setup() {
       // sanity check delay - allows reprogramming if accidently blowing power w/leds
       delay(2000);
       LEDS.setBrightness(1000); //Brillo, para que no me vuele los ojos.
       FastLED.addLeds<WS2801, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
       Serial.begin(9600); // Start serial communication at 9600 bps
       
}

// This function runs over and over, and is where you do the magic to light
// your leds.
void loop() {
// this variable can be changed to potentiometer input
//
val = analogRead(potenciometerPin)/4;    // read the input pin potentiometer, divido por 4 para que no sea tan brusco.
sensorvalue=analogRead(micPin);

Serial.print("SENSADO: ");
Serial.print(sensorvalue);

if (val == 0 )
  val=1;
sensorvalue=sensorvalue/val;
//
// Get MAX
//
if (sensorvalue > lastsensorvalue)
{
  lastsensorvalue=sensorvalue;
}
//
// Get MIN
//
if( sensorvalue < lastminsensorvalue)
{
  lastminsensorvalue=sensorvalue;
}
//
//
int lightNumber= map(sensorvalue,lastminsensorvalue,lastsensorvalue,0,NUM_LEDS);

Serial.print("MAX: ");
Serial.print(lastsensorvalue, DEC);
Serial.print("  ");
Serial.print("LAST: ");
Serial.print(sensorvalue);
Serial.print(" LightNumber: ");
Serial.print(lightNumber, DEC);
Serial.print("MIN: ");
Serial.print("  ");
Serial.print(lastminsensorvalue, DEC);
Serial.print("Sensibility: ");
Serial.println(val, DEC);

if (sensorvalue) { // If val was received
		trailSerialWithLight(lightNumber);
	}
}

void trailSerialWithLight(int valor){
  for(int ledActual = 0; ledActual < NUM_LEDS; ledActual = ledActual + 1) {
         // Turn our current led back to black for the next loop around
         leds[ledActual] = CRGB::Black;
   }
// Wait a little bit
//  delay(1);
// Show the leds (only one of which is set, from above)
//  FastLED.show();
  
  if ( valor > 0 ) {
    for(int ledActual = 0; ledActual < valor ; ledActual = ledActual + 1) {
        if (ledActual < 32)
           leds[ledActual] = CRGB::Blue;
        else        
           leds[ledActual] = CRGB::Yellow;
   }
   
  }
  // Wait a little bit
  // delay(1); 
  FastLED.show();
  
  
 
    // Show the leds (only one of which is set, from above)
   //  FastLED.show();
  // Wait a little bit
 
}

 
void resetValues () {
 if (valant != val) { //si movi el potenciometro recalibro todo
   valant=val;
   Serial.print("Cambio Potenciometro: ");
   Serial.print(valant);
   lastsensorvalue=0;
   sensorvalue=0;
   lastminsensorvalue=1023;
}

}
