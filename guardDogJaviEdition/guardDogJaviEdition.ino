//START:includes
#include <avr/pgmspace.h>
//#include "util.h"
#include <ServoTimer2.h>   

// PIN
int RST = 3;
int CLK = 9;
int DAT = 8;
   
//START:variables
int ledPin        = 13;  // on board LED
int inputPin      = 12;  // input pin for the PIR sensor
int pirStatus     = LOW; // set to LOW (no motion detected)
int pirValue      = 0;   // variable for reading inputPin status
int servoposition = 0;   // starting position of the servo
//END:variables

//START:objects
ServoTimer2 theservo;    // create servo object from the ServoTimer2 class
//END:objects   
   
void setup() {  
  //PIR & SERVO INITIALIZING.
  pinMode(ledPin, OUTPUT);   // set pinMode of the onboard LED to OUTPUT
  pinMode(inputPin, INPUT);  // set PIR inputPin and listen to it as INPUT
  send(0xfff9);//set voice volumn to 7
 theservo.attach(8);        // attach servo motor digital output to pin 7
 Serial.begin(9600);
  randomSeed(analogRead(0)); // seed the Arduino random number generator
    
    pinMode(RST, OUTPUT);
    pinMode(CLK, OUTPUT);
    pinMode(DAT, OUTPUT);
   
   
    digitalWrite(RST, HIGH);
    digitalWrite(CLK, HIGH);
    digitalWrite(DAT, HIGH);
   
    digitalWrite(RST, LOW);
    delay(5);
    digitalWrite(RST, HIGH);
    delay(300);
}

void loop() {
  pirValue = digitalRead(inputPin); // poll the value of the PIR
  
  if (pirValue == HIGH) {           // If motion is detected
    digitalWrite(ledPin, HIGH);  	// turn the onboard LED on
    if (pirStatus == LOW) {			// Trigger motion
      Serial.println("Motion detected");
      
      // Generate a random number between 1 and 5 to match file names
      // and play back the file and move the servo varying degrees 
 /* 
  send(0xfff0);//set voice volumn to 0 (turn off) 
  send(0xfff4);//set voice volumn to 4
  send(0xfff7);//set voice volumn to 7
  send(0xfffe);// pause
  send(0xfffe);//play
*/
      int delayTime=0;
      switch (random(1,5)) {
  /*      case 1: 
          Serial.println("0000.wav");
          theservo.write(1250);          
          send(0x0000);//play file 0000
          theservo.write(1250);
          break;
    */  case 1:
          Serial.println("0001.wav");
          theservo.write(1250);
          delayTime=5000;
          send(0x0001);//play file 0001
          break;
        case 2: 
          Serial.println("0002.wav");
          theservo.write(1400);
          delayTime=6000;
          send(0x0002);//play file 0002
          break;
        case 3: 
          Serial.println("0003.wav");
          theservo.write(1600);
          send(0x0003);//play file 0003
          delayTime=3000;
          break;
        case 4: 
          Serial.println("0004.wav");       
          theservo.write(1850);
          delayTime=7000;
          send(0x0004);//play file 0004
          break;
     /*   case 5: 
          Serial.println("0005.wav");
          theservo.write(2100);
          delayTime=5000;
          send(0x0005);//play file 0005
          break;
          */
      }        
      delay(1500);          // wait a second
      theservo.write(1000); // return the servo to the start position
      delay(delayTime-1500);
      pirStatus = HIGH;     // set the pirStatus flag to HIGH to stop
                            // repeating motion
    }
  } else {
    digitalWrite(ledPin, LOW); // turn the onboard LED off
    if (pirStatus == HIGH){
      Serial.println("No motion");
      send(0xfffe);// pause
      pirStatus = LOW;      // set the pirStatus flag to LOW to 
                            // prepare it for a motion event
    }
  }
}


//Send to mediaShield 

void send(int data)
{
  digitalWrite(CLK, LOW);
  delay(2);
  for (int i=15; i>=0; i--)
  {
    delayMicroseconds(50);
    if((data>>i)&0x0001 >0)
      {
        digitalWrite(DAT, HIGH);
      //  Serial.print(1);
      }
    else
       {
         digitalWrite(DAT, LOW);
      //  Serial.print(0);
       }
    delayMicroseconds(50);
    digitalWrite(CLK, HIGH);
    delayMicroseconds(50);
   
    if(i>0)
    digitalWrite(DAT, LOW);
    else
    digitalWrite(DAT, HIGH);
    delayMicroseconds(50);
   
    if(i>0)
    digitalWrite(CLK, LOW);
    else
    digitalWrite(CLK, HIGH);
  }
 
  delay(20);
}



