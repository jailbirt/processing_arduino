import com.leapmotion.leap.*;
import processing.serial.*;

int width = 640;
int height = 480;
int maxBrushSize = 120;
color canvasColor = 000;
float alphaVal = 10;

Controller leap = new Controller();

Serial myPort;  // Create object from Serial class
int val;        // Data received from the serial port


void setup()
{
   frameRate(120);
   size(width, height);
   background(canvasColor);
   noStroke();
   rectMode(CENTER);
   // First port in the serial list.
   String portName = Serial.list()[0];
   myPort = new Serial(this, portName, 9600);
}


void draw(){
  float valX,valY;
  valX=valY=0;
  Vector tip=new Vector();
  Frame frame = leap.frame();
  Pointable pointer = frame.pointables().frontmost();
  if( pointer.isValid() )
  {
    InteractionBox iBox = frame.interactionBox();
    tip = iBox.normalizePoint(pointer.tipPosition());
  }
  valX = tip.getX() * width;
  valY = height - tip.getY() * height;
  println("valX:"+valX+" valY:"+valY);
  background(51); 
  fill(255, 204);
  rect(valX, height/2, valY/2+10, valY/2+10);
  
  // load up all the values into a byte array
  // then send the full byte array out over serial
  // NOTE: This only works for values from 0-255  
  byte out[] = new byte[2];
  out[0] = byte(int(valX*0.05));
  out[1] = byte(int(valY*0.27777777777));
  println("OUT"+out);
  myPort.write(out); 

}

void keyPressed()
{
   background(canvasColor);
}
