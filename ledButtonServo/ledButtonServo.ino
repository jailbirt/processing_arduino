#include <Servo.h>

Servo servo1; Servo servo2; 
int led[9] = { 3, 4, 5, 6, 7, 8, 9, 10, 11}; // Assign the pins for the leds
int leftChannel = 0;  // left channel input
int left, i;

const int buttonPin = 2;     // the number of the pushbutton pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup()
{
    //Init Servos.
  pinMode(1,OUTPUT);
  servo1.attach(14); //analog pin0
  Serial.begin(19200);
  Serial.println("Servo Ready");
  Serial.begin(9600);
  for (i = 0; i < 9; i++)  // Tell the arduino that the leds are digital outputs
     pinMode(led[i], OUTPUT);
}

void loop()
{

  
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin); // check if the pushbutton is pressed. if it is, the buttonState is HIGH:
  Serial.print(".");
  int incomingByte=0;
    
  if (buttonState == HIGH) { 
    incomingByte=Serial.read();
    Serial.println("Button Status Is: "+buttonState);
    Serial.print(
    incomingByte,DEC); 
    servo1.write(180);
    for (i = 0; i < 9; i++) // turn on the leds up to the volume level
    {
      digitalWrite(led[i], HIGH);
      delay(100);  
      digitalWrite(led[i], LOW);
       
     }
     servo1.write(0);  
  }     
   
}

