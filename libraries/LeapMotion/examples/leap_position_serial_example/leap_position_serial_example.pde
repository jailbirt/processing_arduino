/*

    Leap Motion library for Processing.
  
*/
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentHashMap;

import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Finger;
import com.leapmotion.leap.Frame;
import com.leapmotion.leap.Hand;
import com.leapmotion.leap.Tool;
import com.leapmotion.leap.Vector;
import com.leapmotion.leap.processing.LeapMotion;

LeapMotion leapMotion;

ConcurrentMap<Integer, Integer> fingerColors;
ConcurrentMap<Integer, Integer> toolColors;
ConcurrentMap<Integer, Vector> fingerPositions;
ConcurrentMap<Integer, Vector> toolPositions;

import processing.serial.*;

Serial myPort;  // Create object from Serial class
int val;        // Data received from the serial port


void setup()
{
  size(640, 360); 
  background(20);
  frameRate(60);
  ellipseMode(CENTER);

  leapMotion = new LeapMotion(this);
  fingerColors = new ConcurrentHashMap<Integer, Integer>();
  toolColors = new ConcurrentHashMap<Integer, Integer>();
  fingerPositions = new ConcurrentHashMap<Integer, Vector>();
  toolPositions = new ConcurrentHashMap<Integer, Vector>();
  
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);

}

void draw()
{
  fill(20);
  rect(0, 0, width, height);
  int[] fingersArray = new int[10]; //max Fingers.
  float yPos=0;
  
  for (Map.Entry entry : fingerPositions.entrySet())
  {
    Integer fingerId = (Integer) entry.getKey();
    Vector position = (Vector) entry.getValue();
    fill(fingerColors.get(fingerId));
    noStroke();
    yPos=leapMotion.leapToSketchY(position.getY());
    ellipse(leapMotion.leapToSketchX(position.getX()), yPos , 24.0, 24.0);
    println("fingerID"+fingerId);
    fingersArray[fingerId]=int(leapMotion.leapToSketchX(position.getX()));
  }
  
  for (Map.Entry entry : toolPositions.entrySet())
  {
    Integer toolId = (Integer) entry.getKey();
    Vector position = (Vector) entry.getValue();
    fill(toolColors.get(toolId));
    noStroke();
    ellipse(leapMotion.leapToSketchX(position.getX()), leapMotion.leapToSketchY(position.getY()), 24.0, 24.0);
  }
     
     sendPositionsToSerial(fingersArray[0],fingersArray[1],fingersArray[2],fingersArray[3],fingersArray[4],fingersArray[5],
                           fingersArray[6],fingersArray[7],fingersArray[8],fingersArray[9],yPos);  
}

void onFrame(final Controller controller)
{
  Frame frame = controller.frame();
  for (Finger finger : frame.fingers())
  {
    int fingerId = finger.id();
    color c = color(random(0, 255), random(0, 255), random(0, 255));
    fingerColors.putIfAbsent(fingerId, c);
    fingerPositions.put(fingerId, finger.tipPosition());
  }
  for (Tool tool : frame.tools())
  {
    int toolId = tool.id();
    color c = color(random(0, 255), random(0, 255), random(0, 255));
    toolColors.putIfAbsent(toolId, c);
    toolPositions.put(toolId, tool.tipPosition());
  }

  // todo:  clean up expired finger/toolIds
}

void sendPositionsToSerial ( int valX1, int valX2, int valX3, int valX4, int valX5, int valX6, int valX7, int valX8, int valX9, int valX10, float Y){
  // load up all the values into a byte array
  // then send the full byte array out over serial
  // NOTE: This only works for values from 0-255  
  byte out[] = new byte[2];
  out[0] = byte(int(valX1*0.05));
  out[1] = byte(int(valX2*0.05));
  out[2] = byte(int(valX3*0.05));
  out[3] = byte(int(valX4*0.05));
  out[4] = byte(int(valX5*0.05));
  out[5] = byte(int(valX6*0.05));
  out[6] = byte(int(valX7*0.05));
  out[7] = byte(int(valX8*0.05));
  out[8] = byte(int(valX9*0.05));
  out[9] = byte(int(valX10*0.05));
  out[10] = byte((100-int(Y*0.27777777777))*3);
  // println("Para X:"+X+" Y:"+Y+" OUT:"+out);
  myPort.write(out); 
}
