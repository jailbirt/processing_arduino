/**
 * Mouse 2D. 
 * 
 * Moving the mouse changes the position and size of each box. 
 */
 
import processing.serial.*;

Serial myPort;  // Create object from Serial class
int val;        // Data received from the serial port

void setup() 
{
  size(640, 360); 
  noStroke();
  rectMode(CENTER);
  // First port in the serial list.
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 9600);
}

void draw() 
{   
  background(51); 
  fill(255, 204);
  rect(mouseX, height/2, mouseY/2+10, mouseY/2+10);
  fill(255, 204);
  int inverseX = width-mouseX;
  int inverseY = height-mouseY;
  rect(inverseX, height/2, (inverseY/2)+10, (inverseY/2)+10);
 
  // load up all the values into a byte array
  // then send the full byte array out over serial
  // NOTE: This only works for values from 0-255  
  byte out[] = new byte[2];
  out[0] = byte(int(pmouseX*0.05));
  out[1] = byte(int(pmouseY*0.27777777777));
  println("OUT"+out);
  myPort.write(out); 
}

