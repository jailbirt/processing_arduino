
void decideSegunHumedad (){
  // read the value from the sensor:
  soilSensorValue=analogRead(sensorPin);
  delay(1000);                  // waits for a second 
  // map the sensor range to a range of three options:
  int range = map(  soilSensorValue, sensorMin, sensorMax, 0, 2);
  

  // do something different depending on the 
  // range value:
  switch (range) {
  case 0:    // seco
    Serial.print("seco: ");
    break;
  case 1:    // humedo
    Serial.print("humedo: ");
    break;
  case 2:    // la cagaste, error fatal.
    Serial.print("mojado: ");
    break; 
  }
  Serial.println(soilSensorValue);
}
