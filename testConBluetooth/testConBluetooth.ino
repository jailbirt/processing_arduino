/*
simple LED test
*/

char val;         // variable to receive data from the serial port
int ledpin = 2;  // LED connected to pin 2 (on-board LED)
//Soil Sensor
int sensorPin = A0;    // select the input pin for the soil sensor.
int soilSensorValue = 0;  // variable to store the value coming from the sensor
// sensor de tierra constants won't change:
const int sensorMin = 0;      // sensor minimum
const int sensorMax = 650;    // sensor maximum
// End Soil Sensor
//DHT11 Sensor
#include <dht.h>
dht DHT;
#define DHT11_PIN 5
//END DHT11 Sensor

void setup()
{
  pinMode(ledpin = 13, OUTPUT);  // pin 13 (on-board LED) as OUTPUT
  Serial.begin(9600);       // start serial communication 
}
 
void loop() {
  if( Serial.available() )       // if data is available to read
    val = Serial.read();         // read it and store it in 'val'
  else {
   decideSegunHumedad();
   informaTemperaturaYhumedadAmbiente();
  }
  if( val == '0' )               // if '0' was received led 13 is switched off
  {
   digitalWrite(ledpin, LOW);    // turn Off pin 13 off
   delay(1000);                  // waits for a second   
   Serial.println("13 off");
  }

if( val == '1' )               // if '1' was received led 13 on
 {
    digitalWrite(ledpin = 13, HIGH);  // turn ON pin 13 on
    delay(1000);                  // waits for a second
    Serial.println("13 on");
 }
  
}
