  
/*
This code is show how Arduino Wave Module works with Arduino.
Code is not optimized. Any improving work on it is encouraged.
*/
int RST = 3;
int CLK = 9;
int DAT = 8;
   
   
void setup() {  
   
   
    pinMode(RST, OUTPUT);
    pinMode(CLK, OUTPUT);
    pinMode(DAT, OUTPUT);
   
   
    digitalWrite(RST, HIGH);
    digitalWrite(CLK, HIGH);
    digitalWrite(DAT, HIGH);
   
    digitalWrite(RST, LOW);
    delay(5);
    digitalWrite(RST, HIGH);
    delay(300);
}

void loop() {

  send(0x0000);//play file 0000
  delay(10000);//delay 10 seconds
 
  send(0x0001);//play file 0001
  delay(10000);//delay 10 seconds
 
  send(0x0002);//play file 0002
  delay(10000);//delay 10 seconds
 
  send(0xfff0);//set voice volumn to 0 (turn off)
  delay(3000);
 
  send(0xfff4);//set voice volumn to 4
  delay(3000);
 
  send(0xfff7);//set voice volumn to 7
  delay(3000);
 
  send(0xfffe);// pause
  delay(5000);
  send(0xfffe);//play
 
 
 
//  while(1);
}
void send(int data)
{
  digitalWrite(CLK, LOW);
  delay(2);
  for (int i=15; i>=0; i--)
  {
    delayMicroseconds(50);
    if((data>>i)&0x0001 >0)
      {
        digitalWrite(DAT, HIGH);
        Serial.print(1);
      }
    else
       {
         digitalWrite(DAT, LOW);
        Serial.print(0);
       }
    delayMicroseconds(50);
    digitalWrite(CLK, HIGH);
    delayMicroseconds(50);
   
    if(i>0)
    digitalWrite(DAT, LOW);
    else
    digitalWrite(DAT, HIGH);
    delayMicroseconds(50);
   
    if(i>0)
    digitalWrite(CLK, LOW);
    else
    digitalWrite(CLK, HIGH);
  }
 
  delay(20);
}

